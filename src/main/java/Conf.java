import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Conf {

    private Map<String, ConfElement> confLines;


    public Conf() {
        confLines = new HashMap<>();
    }

    public boolean addLine(String classpathCalled, String classpathUsed, Map<String, String> args, String initTyp, boolean singleton) {
        if (confLines.containsKey(classpathCalled))
            return false;
        ConfElement elem = new ConfElement(classpathCalled, classpathUsed, args, initTyp, singleton);
        confLines.put(classpathCalled, elem);
        return true;
    }

    public boolean check(){
        for (ConfElement confLine: confLines.values()) {
            try {
                Class c = Class.forName(confLine.getClasspathUsed());
            } catch (ClassNotFoundException e) {
                return false;
            }
        }
        return true;
    }

    public ConfElement getLine(String classpathCalled) {
        if (confLines.containsKey(classpathCalled))
            return confLines.get(classpathCalled);
        return null;
    }

    public List<String> getSingletons() {
        List<String> tmp = new ArrayList<>();
        for (ConfElement confLine: confLines.values()) {
            if(confLine.isSingleton()) {
                tmp.add(confLine.getClasspathUsed());
            }
        }
        return tmp;
    }

}
