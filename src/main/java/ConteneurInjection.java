import com.sun.org.apache.xpath.internal.operations.Bool;
import org.json.JSONObject;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class ConteneurInjection {
    String filePath;
    Conf conf;
    ParseurFichier parseurFichier;
    Map<String, Object> singletons;


    public ConteneurInjection(String filePath) {

        this.filePath = filePath;
        parseurFichier = new ParseurJson();
        singletons = new HashMap<>();
        conf = new Conf();

        parseurFichier.readFile(filePath);
        parseurFichier.fillConf(conf);

        List<String> tmp = conf.getSingletons();
        for (String elem: tmp) {
            singletons.put(elem, null);
        }
    }

    private void setAttributes(Object o, String args){
        JSONObject jsonObject = new JSONObject(args);
    }

    Object getInstance(String classPathCalled) {
        ConfElement elem = conf.getLine(classPathCalled);
        if (elem != null) {
            try {
                Class c = Class.forName(elem.getClasspathUsed());

                /*Object obj =  createObject(c);

                initializeObject(obj, elem);*/
                Object obj = initializeObject2(c, elem);

                return obj;
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("Erreur de creation\n");
            return null;
        }
        System.out.println("Erreur car non trouve\n");
        return null;
    }

    private Object createObject(Class className) throws IllegalAccessException, InstantiationException {
        Object tmp;
        if(singletons.containsKey(className.getName())) {
            tmp = singletons.get(className.getName());
            if (tmp == null) {
                tmp = className.newInstance();
                singletons.put(className.getName(), tmp);
            }
        }
        else {
            tmp = className.newInstance();
        }
        return tmp;
    }

    private Object getPrimitive(Field field, String value) {
        Object arg = null;

        switch (field.getType().getName()) {
            case "boolean":
                arg = Boolean.parseBoolean(value);
                break;
            case "byte":
                arg = Byte.parseByte(value);
                break;
            case "char":
                arg = value.charAt(0);
                break;
            case "short":
                arg = Short.parseShort(value);
                break;
            case "int":
                arg = Integer.parseInt(value);
                break;
            case "long":
                arg = Long.parseLong(value);
                break;
            case "float":
                arg = Float.parseFloat(value);
                break;
            case "double":
                arg = Double.parseDouble(value);
                break;
        }

        return arg;
    }

    private Object getAttribute(Object obj, Field field, Map<String, String> confAttributes) throws Exception{
        Object arg = null;

        if(field.getType().isPrimitive()){
            String value = confAttributes.get(field.getName());
            arg = getPrimitive(field, value);
        }
        else {
            if(singletons.containsKey(field.getName())){
                arg=singletons.get(field.getName());
            }
            else {

            }
        }

        return arg;
    }

    private void initializeObject(Object obj, ConfElement elem) {
        Map<String, String> confAttributes = elem.getArgs();
        Field[] objAttributes = obj.getClass().getDeclaredFields();
        switch (elem.getInitTyp()) {
            case "set" :
                for (Field field: objAttributes ) {
                    String methodsName = "set" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
                    if ( confAttributes.containsKey(field.getName())) {
                        try {
                            Method method = obj.getClass().getDeclaredMethod(methodsName, field.getType());
                            method.invoke(obj, getAttribute(obj, field, confAttributes));
                        } catch (Exception e){
                            System.out.println("Error Setter or Field does not exist");
                        }
                    }

                }
                break;
            case "constructor" :
                System.out.println("Constructor");
                break;
            case "value" :
                System.out.println("Value");
                break;
        }
    }

    private Object initializeObject2(Class className, ConfElement elem) throws InstantiationException, IllegalAccessException {
        Map<String, String> confAttributes = elem.getArgs();
        Object obj = null;
        Field[] objAttributes;
        switch (elem.getInitTyp()) {
            case "set" :
                obj = createObject(className);
                objAttributes = obj.getClass().getDeclaredFields();
                for (Field field: objAttributes ) {
                    String methodsName = "set" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
                    if ( confAttributes.containsKey(field.getName())) {
                        try {
                            Method method = obj.getClass().getDeclaredMethod(methodsName, field.getType());
                            method.invoke(obj, getAttribute(obj, field, confAttributes));
                        } catch (Exception e){
                            System.out.println("Error Setter or Field does not exist");
                        }
                    }

                }
                break;
            case "constructor" :
                // WIP
                Constructor[] constructors = className.getConstructors();

                // Select the right constructor (0 in the beginning)
                Constructor<Object> constructor = constructors[0];
                List<Object> args = new ArrayList<>();
                objAttributes = className.getDeclaredFields();

                for (Field field: objAttributes ) {
                    if ( confAttributes.containsKey(field.getName())) {
                        try {
                            args.add(getAttribute(obj, field, confAttributes));
                        } catch (Exception e){
                            System.out.println("Error Field does not exist");
                        }
                    }
                }

                try {
                    obj = constructor.newInstance((Object[]) args.toArray());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                System.out.println("Constructor");
                break;

            case "value" :
            default :
                obj = createObject(className);
                objAttributes = obj.getClass().getDeclaredFields();
                for (Field field: objAttributes ) {
                    if ( confAttributes.containsKey(field.getName())) {
                        try {
                            boolean tmp = field.isAccessible();
                            field.setAccessible(true);

                            field.set(obj, getAttribute(obj, field, confAttributes));

                            field.setAccessible(tmp);
                        } catch (Exception e){
                            System.out.println("Error Field does not exist");
                        }
                    }
                }
                break;
        }
        return obj;
    }

}
