import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConfElement {

    private String classpathCalled;
    private String classpathUsed;
    private Map<String, String> args;
    private String initTyp;
    private boolean singleton;

    public ConfElement(String classpathCalled, String classpathUsed, Map<String, String> args, String initTyp, boolean singleton) {
        this.classpathCalled = classpathCalled;
        this.classpathUsed = classpathUsed;

        if(args != null)
            this.args = new HashMap<>(args);
        else
            this.args = new HashMap<>();

        this.initTyp = initTyp;
        this.singleton = singleton;
    }

    public boolean isSingleton() {
        return singleton;
    }

    public String getClasspathCalled() {
        return classpathCalled;
    }

    public String getClasspathUsed() {
        return classpathUsed;
    }

    public String getInitTyp() {
        return initTyp;
    }

    public Map<String, String> getArgs() {
        return args;
    }
}
