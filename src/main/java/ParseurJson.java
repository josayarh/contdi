import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ParseurJson implements ParseurFichier {
    JSONObject jsonReader;

    public void readFile(String filePath) {
        try {
            String filecontent = new String(Files.readAllBytes(Paths.get(filePath)));
            jsonReader = new JSONObject(filecontent);

        } catch (FileNotFoundException fe){
            System.out.println("Error : File not found, check file path of injection description file.");
        } catch (IOException ioe){
            System.out.println("Error : File not found, check file path of injection description file.");
        }

    }

    public JSONObject getJsonReader() {
        return jsonReader;
    }

    public void fillConf(Conf conf) {
        Iterator<String> keys = jsonReader.keys();

        // Sur chaque element
        while(keys.hasNext()) {
            String key = keys.next();
            if (jsonReader.get(key) instanceof JSONObject) {
                JSONObject line = (JSONObject) jsonReader.get(key);
                JSONObject attributes = (JSONObject) line.get("Attributes");
                Map<String, String> args = null;

                if(attributes != null){
                    Iterator<String> attrKeys = attributes.keys();
                    args = new HashMap<>();
                    while(attrKeys.hasNext()){
                        String attrKey = attrKeys.next();
                        args.put(attrKey, attributes.get(attrKey).toString());
                    }
                }

                //System.out.println(key + "\n");

                // Sur chaque attribut d'un element
                /*
                Iterator<String> keys2 = line.keys();
                while (keys2.hasNext()) {
                    String key2 = keys2.next();
                    if (line.get(key2) instanceof JSONObject) {
                        System.out.println("\t[" + key2 + " : " + line.get(key2) + "]\n");
                    }
                    else {
                        System.out.println("\t" + key2 + " : " + line.get(key2) + "\n");
                    }
                }
                */

                String setMethod = "value";

                if (line.has("AttributesSetMethod")) {
                    setMethod = (String) line.get("AttributesSetMethod");
                }
                conf.addLine(key, (String) line.get("InheritancePath"), args, setMethod, (Boolean) line.get("isSingleton"));
            }
        }
    }
}
