public interface ParseurFichier {

    void readFile(String filePath);
    void fillConf(Conf conf);
}
