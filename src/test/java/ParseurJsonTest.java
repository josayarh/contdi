import org.json.JSONObject;

import static org.junit.jupiter.api.Assertions.*;

class ParseurJsonTest {

    @org.junit.jupiter.api.Test
    void readFile() {
        String filename = "./randomJson.json";
        ParseurFichier pf = new ParseurJson();
        String sentence;

        pf.readFile(filename);

        JSONObject jsonObject = ((ParseurJson) pf).getJsonReader();
        sentence = jsonObject.get("sentence").toString();
        assertEquals(sentence, "false");
    }
}