import static org.junit.jupiter.api.Assertions.*;

class ConteneurInjectionTest {

    @org.junit.jupiter.api.Test
    void createObject() {
        ConteneurInjection di = new ConteneurInjection("./ConfigurationFile/config.json");

        God test = (God) di.getInstance(God.class.getName());

        if (test != null) {
            assertEquals(test.getClass().getName(), DoctorDoom.class.getName());
            assertEquals(true, test.isOmnipotent());
        }
        else {
            fail("Object not instantiated");
        }
    }

    @org.junit.jupiter.api.Test
    void createSingleton() {
        ConteneurInjection di = new ConteneurInjection("./ConfigurationFile/config.json");

        InterfaceSingleton test = (InterfaceSingleton) di.getInstance(InterfaceSingleton.class.getName());

        if (test != null) {
            test.setValue(42);
            InterfaceSingleton test2 = (InterfaceSingleton) di.getInstance(InterfaceSingleton.class.getName());
            assertEquals(test2.getValue(), 42);
        }
        else {
            fail("Object not instantiated");
        }
    }

    @org.junit.jupiter.api.Test
    void initialyzeObject() {
        ConteneurInjection di = new ConteneurInjection("./ConfigurationFile/config.json");

        God test = (God) di.getInstance(God.class.getName());
        InitialyzeConstructor test2 = (InitialyzeConstructor) di.getInstance(InitialyzeConstructor.class.getName());
        InitialyzeValues test3 = (InitialyzeValues) di.getInstance(InitialyzeValues.class.getName());

        if (test != null) {
            assertEquals(true, test.isOmnipotent());
            assertEquals(42, test2.getMyValue());
            assertEquals(43, test3.getMyValue());
        }
        else {
            fail("Object not instantiated");
        }
    }
}