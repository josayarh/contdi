public class DoctorDoom implements God {
    boolean omnipotent;
    boolean isDoombot;

    public DoctorDoom() {
        omnipotent = false;
    }

    public void setOmnipotent(boolean omnipotent) {
        this.omnipotent = omnipotent;
    }

    @Override
    public boolean isOmnipotent() {
        return omnipotent;
    }
}
