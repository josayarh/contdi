public interface InterfaceSingleton {
    int getValue();

    void setValue(int a);
}
