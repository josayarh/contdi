public class ObjectSingleton implements InterfaceSingleton{
    private int value = 6;

    public int getValue() {
        return value;
    }

    public void setValue(int a) {
        value = a;
    }
}
