public class InitialyzeConstructor {

    private int myValue;

    public InitialyzeConstructor(int myValue) {
        this.myValue = myValue;
    }

    public int getMyValue() {
        return myValue;
    }
}
